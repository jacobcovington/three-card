import * as THREE from './three.js';
import { OBJLoader } from './OBJLoader.js';

const bubbles = window.location.search.includes('bubbles');

const OBJ_URL = 'card/card.obj';
const TEXTURE_URL = bubbles ? 'card/bubbles.jpg' : 'card/brex.png';

let container;
let camera, scene, renderer;
let object;

let mouseX = 0;
let mouseY = 0;
let windowHalfX = window.innerWidth / 2;
let windowHalfY = window.innerHeight / 2;

function onWindowResize() {
  windowHalfX = window.innerWidth / 2;
  windowHalfY = window.innerHeight / 2;
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();
  renderer.setSize(window.innerWidth, window.innerHeight);
}

function onDocumentMouseMove(event) {
  mouseX = (event.clientX - windowHalfX) / 2;
  mouseY = (event.clientY - windowHalfY) / 2;
}

function animate() {
  if (object) {
    object.rotation.z -= 0.005;
    object.rotation.x -= 0.005;
    object.rotation.y += 0.005;
  }
  requestAnimationFrame(animate);
  render();
}

function render() {
  camera.position.x += (-mouseX - camera.position.x) * 0.007;
  camera.position.y += (mouseY - camera.position.y) * 0.007;
  camera.lookAt(scene.position);
  renderer.render(scene, camera);
}

function init() {
  container = document.createElement('div');
  document.body.appendChild(container);
  const aspect = window.innerWidth / window.innerHeight;
  camera = new THREE.PerspectiveCamera(45, aspect, 1, 2000);
  camera.position.z = 50;

  // scene
  scene = new THREE.Scene();
  scene.background = new THREE.Color(0xf7eefb);

  const ambientLight = new THREE.AmbientLight(0xcccccc, 0.4);
  scene.add(ambientLight);

  const pointLight = new THREE.PointLight(0xffffff, 0.8);
  camera.add(pointLight);
  scene.add(camera);

  // manager
  function loadModel() {
    object.traverse(function (child) {
      if (child.isMesh) {
        if (child.name === 'Box001') {
          child.material.map = texture;
        }
      }
    });

    object.position.z = -100;
    object.rotation.x = Math.PI / 2;
    scene.add(object);
  }

  const manager = new THREE.LoadingManager(loadModel);
  manager.onProgress = function (item, loaded, total) {
    console.log(item, loaded, total);
  };

  // texture
  const textureLoader = new THREE.TextureLoader(manager);
  const texture = textureLoader.load(TEXTURE_URL);

  // model
  function onProgress(xhr) {
    if (xhr.lengthComputable) {
      var percentComplete = (xhr.loaded / xhr.total) * 100;
      console.log(
        'model ' + Math.round(percentComplete, 2) + '% downloaded',
      );
    }
  }

  const onLoad = (obj) => {
    object = obj;
  };
  const onError = () => {};
  const loader = new OBJLoader(manager);
  loader.load(OBJ_URL, onLoad, onProgress, onError);

  renderer = new THREE.WebGLRenderer();
  renderer.setPixelRatio(window.devicePixelRatio);
  renderer.setSize(window.innerWidth, window.innerHeight);
  container.appendChild(renderer.domElement);

  document.addEventListener('mousemove', onDocumentMouseMove, false);

  window.addEventListener('resize', onWindowResize, false);
}

init();
animate();
